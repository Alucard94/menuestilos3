package com.example.user.menuestilos31;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by User on 31/01/2016.
 */
public class AlertDialog extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Builder builder = new Builder(getActivity());
        builder.setMessage("Nombre: Augusto Pecho Chávez\nCódigo: 20124061F\nE-mail: augusto.pecho.c@uni.pe" +
                "\nEspecialidad: Ciencias de la Computación")
                .setTitle("Información")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }
}